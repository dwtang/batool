__author__ = "TangDW"

from itertools import combinations as cb
from itertools import combinations_with_replacement as cr
import math as mt


# utility profile
profile = []
with open("profile.txt", 'r') as file:
    line = file.readline().split()
    blength, bwidth = int(line[0]), int(line[1])
    file.readline()
    for line in file:
        profile += [[float(w) for w in line.split()]]

# parameters of the "arena"
bgoods = len(profile[0])
bagents = len(profile)
bk = 4  # complementarity
sj = [11 for i in range(bgoods)]

# generate the list of feasible bundles
bundle_list = []
for i in range(bk):
    bundle_list += list(cr(range(bgoods), i+1))
bundle_list_size = len(bundle_list)

def utility(agent, bundle, shh=False):
    """Calculates the utility of a bundle b for agent i"""
    ub = 0
    uvec = profile[agent]
    #for j in set(bundle):
        #ub += uvec[j] * mt.pow(bundle.count(j), 1.5)
    for j,k in cb(set(bundle), 2):
        if abs(j - k) == bwidth or abs(j - k) == 1 and j // blength == k // blength:
            if not shh:
                print("Ajacent!", j, k)
            ub += 0.5 * (uvec[j] + uvec[k]) * min(bundle.count(j), bundle.count(k))
    for j in bundle:
        ub += uvec[j]
    if not shh:
        print("Agent: {1}, Bundle: {0}, utility: {2}".format(bundle, agent, ub))
    return ub