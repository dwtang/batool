#!/usr/bin/env python

# usage: python genprofile.py l w k fn
# (arena is l x w, k is number of complimentarity)
# (fn is the name of output file) 

from numpy import random as rd
import sys

if len(sys.argv) < 5:
	print('Not enough arguments!')
	sys.exit()

bk = int(sys.argv[3])

blength, bwidth = int(sys.argv[1]), int(sys.argv[2])

bgoods = blength * bwidth
bpairs = 4 * bgoods - 2 * blength - 2 * bwidth 
bagent = bgoods * 3
sj = 3*bk-1

s = "{0} {1} {2} {3}\n".format(blength, bwidth, bk, sj)
for i in range(bagent):
	s += "\n"
	for j in range(bgoods):
		s += "{0:.2f}\t".format(rd.rand() * 50 + 50)

s += "\n    # This file is auto-gen by genprofile.py"

for i in range(bagent):
	s += "\n"
	for j in range(bpairs):
		s += "{0:.2f}\t".format(rd.rand() * 20 + 20)

with open(sys.argv[4], 'w') as g:
	g.write(s)
