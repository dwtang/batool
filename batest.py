from balloc import *

from itertools import combinations as cb
from itertools import combinations_with_replacement as cr
import numpy as np
import time, sys

def batest():
    """Test function for bacalc"""
    profile = []
    with open("profile.txt", 'r') as file:
        line = file.readline().split()
        blength, bwidth = int(line[0]), int(line[1])
        file.readline()
        for line in file:
            profile += [[float(w) for w in line.split()]]

    # parameters of the "arena"
    bgoods = len(profile[0])
    bagents = len(profile)
    bk = 4  # complementarity
    sj = [11 for i in range(bgoods)]

    # generate the list of feasible bundles
    bundle_list = []
    for i in range(bk):
        bundle_list += list(cr(range(bgoods), i+1))
    bundle_list_size = len(bundle_list)

    # ---------------------------
    t0 = time.time()
    adjlist = []
    for l in range(blength):
        for w in range(bwidth-1):
            gr = l*bwidth+w
            adjlist.append((gr, gr+1))
            adjlist.append((gr+1, gr))
    for l in range(blength-1):
        for w in range(bwidth):
            gr = l*bwidth+w
            adjlist.append((gr, gr+bwidth))
            adjlist.append((gr+bwidth, gr))

    adjlist.sort()
    bpairs = len(adjlist)

    ugmat = np.array(profile)

    Bmat = np.zeros((bgoods, bundle_list_size), dtype='int')
    for b in range(bundle_list_size):
        for j in range(bgoods):
            Bmat[j, b] = bundle_list[b].count(j)

    cmat = np.zeros((bagents, bpairs), dtype='float32')
    dBmat = np.zeros((bpairs, bundle_list_size), dtype='int')
    for p in range(bpairs):
        pair = adjlist[p]
        for i in range(bagents):
            cmat[i, p] = 0.3*ugmat[i, pair[0]]
        for b in range(bundle_list_size):
            dBmat[p, b] = Bmat[pair[0], b] - Bmat[pair[1], b]
            if dBmat[p, b] < 0:
                dBmat[p, b] = 0

    umat = np.dot(ugmat, Bmat) - np.dot(cmat, dBmat)
    print('Calculating Utility Profile: elapsed time = {0:.2f} sec'.format(time.time()-t0))
    # ---------------------------

    Onemat = [[0]*(i*bundle_list_size) + [1]*bundle_list_size + 
     [0]*(bagents-i-1)*bundle_list_size for i in range(bagents)]
    Twomat = [[b.count(j) for b in bundle_list]*bagents for j in range(bgoods)]
    Amat = Onemat + Twomat
    bvec = [1]*bagents + sj

    wdelta, pdelta, tol = 1e-4, 1e-2, 1e-5

    bap = {"bagents":bagents, "bgoods":bgoods,
                "bundle_list_size":bundle_list_size,
                "umat":umat, "Amat":Amat, "bvec":bvec, "k":bk,
                "bundle_list":bundle_list, "wdelta":wdelta, "pdelta":pdelta, "tol":tol,
                "method":'ECOS'}

    # return
    for chance in range(100):
        try:
            Xmat, la, xvec, pvec, uval = bacalc(**bap)
        except SolverError as err:
            print('Error: ', err)
            print('Run it again ...')
            continue
        else:
            break
    else:
        print('All trial failed, unlucky of you\nRun the program again.')
        sys.exit()

    print("\nFinal solution:")
    for i in range(bagents * bundle_list_size):
        if xvec[i] > tol:
            print("Agent {0} is allocated {1:.3f} portion of bundle: {2}".format(i // bundle_list_size, xvec[i], bundle_list[i % bundle_list_size]))
    print("Total Utility: ", uval)
    tolu = wdelta * np.max(umat) + tol
    checksol(xvec, pvec, umat, bagents, bgoods, bundle_list_size, bundle_list, bk, tolu, tol)


def checksol(xvec, pvec, umat, bagents, bgoods, bundle_list_size, bundle_list, k, tolu, tolx, verbose=True):
    """Check whether the solution is reasonable"""
    print("### Checksol")
    print('Tolerance: ', tolu)
    plausible = True
    for i in range(bagents):
        alphai = 0
        argmax_alphai = -1
        ichoice = -1
        ipayoff = 0
        for k in range(bundle_list_size):
            b = bundle_list[k]
            payoff = umat[i][k] - sum([b.count(j) * pvec[j] for j in range(bgoods)])
            #print("Agent {0} bundle {1} payoff: {2}".format(i, b, payoff))
            if payoff > alphai + tolx:
                argmax_alphai = k
                alphai = payoff
            if xvec[i * bundle_list_size + k] > 1 - tolx:
                ichoice = k
                ipayoff = payoff
        b = bundle_list[ichoice] if ichoice >= 0 else ()
        if ipayoff > alphai - tolu:
            if verbose:
                print("Agent {agent} acts approximately rationally and buys {bundle}".format(agent=i, bundle=b))
        else:
            plausible = False
            if verbose:
                bset = bundle_list[argmax_alphai]
                print("Agent {agent} acts IRRATIONALLY and buys {bundle}, she could better choose among bundle {bset}".format(agent=i, bundle=b, bset=bset))

    if not verbose:
        if plausible:
            print('### Good!')
        else:
            print('### SOMETHING WRONG!')

if __name__ == '__main__':
    batest()