__author__ = "TangDW"

import numpy as np
import scipy as sp
import math as mt
from numpy import linalg as la
from scipy import optimize as opt
from copy import deepcopy as dcp

from badefs import *

def bacalc():
    """Do some real stuff: 
        Calculate the (approximate) optimal allocation
        Calculate the market clearing price"""
    # Set tolerance
    tol = 1e-12
    # Set LP matrices and vectors
    # negate the utility vec since linprog is minimising
    uvec = [-utility(i, b) for i in range(bagents) for b in bundle_list]
    # First part of Amat: Demand constraints
    Amat = [[0] * (bundle_list_size * i) + [1] * (bundle_list_size) + [0] * (bundle_list_size * (bagents - i - 1)) for i in range(bagents)]
    # Second part of Amat: Supply constraints
    Amat += [[b.count(j) for b in bundle_list] * bagents for j in range(bgoods)]
    bvec = [1] * bagents + sj
    # Bounds [0, Inf) are default in linprog

    nst, nx = len(bvec), len(uvec) # Number of constraints, Number of variables

    # print("rank of Amat: ", la.matrix_rank(Amat))

    # Linear Programming: Solve primal and dual
    
    # Dual calculation function (called by linprog)
    pvec = [[]]
    def calcdual(xk, **kwargs):
        if kwargs["complete"]:
            T = kwargs["tableau"]
            invB = T[:nst, nx:nx + nst]   # Inverse matrix
            bvvec = uvec + [0] * nst    # [uvec 0] is the bound vector for dual variables
            cbvec = [[0] * nst]  # utility row vector corresponding to final basic variables 
            # Align the utility value according to the order of basic variables in column
            for bv in kwargs["basis"]:
                for i in range(nst):
                    if T[i, bv] > 1 - tol:
                        cbvec[0][i] = -bvvec[bv] # negate it back, here we use positive utility
                        break
            dvec = np.dot(cbvec, invB) # dual vec = c^B * B^(-1)
            pvec[0] = dvec[0][bagents:] # price subvec of dual variable vec
            # Calculate the dual objfunc value, compare it with primal objfunc value
            dualval = np.inner(bvec, dvec[0])
            #print("Dual solution: ", dvec[0])
            print("Dual value: ", dualval)

    print("Solving LP using simplex method ...")
    res = opt.linprog(uvec, A_ub=Amat, b_ub=bvec, callback=calcdual)
    xvec = res.x
    slvec = res.slack

    printsol(res)

    # Print Market Clearing Price
    pvec = pvec[0]
    for j in range(bgoods):
        print("Price of good {0} is {1:.2f}".format(j, pvec[j]))

    #return xvec, pvec
    # Iterative rounding
    print("Iterative Rounding ...")
    xunfixed_num = nx
    xunfixed_list = list(range(xunfixed_num))
    Aeqmat, beqvec = [], []
    bagents_unsatnum = bagents
    part_Amat, part_uvec, part_bvec = dcp(Amat), dcp(uvec), dcp(bvec)
    part_xvec, part_slvec = list(xvec), list(slvec)
    while True:
        solved = True   # True if we have got an intergral solution
        allfrac = True  # True if all entries are fractional
        
        #print("part_xvec: ", part_xvec)
        # Precheck if integral
        for iii in range(xunfixed_num):
            rem = part_xvec[iii] - np.round(part_xvec[iii])
            isint = (-tol <= rem <= tol)
            if not isint:
                solved = False
                break
            else:
                print("xunfixed_{0} is int!".format(iii))
        
        # If all entries integral, then done!               
        if solved:
            break
        
        ii = 0
        # Fix integral x
        while ii < xunfixed_num:
            #print("ii: {0}, part_xvec[ii]: {1}".format(ii, part_xvec[ii]))
            if -tol <= (part_xvec[ii] - np.round(part_xvec[ii])) <= tol:
                # Reduce the problem
                print('if')
                allfrac = False
                xunfixed_num -= 1
                del xunfixed_list[ii]
                for k in range(len(part_bvec)):
                    part_bvec[k] -= part_Amat[k][ii] * np.round(part_xvec[ii])
                    del part_Amat[k][ii]
                for k in range(len(beqvec)):
                    beqvec[k] -= Aeqmat[k][ii] * np.round(part_xvec[ii])
                    del Aeqmat[k][ii]
                # Eliminate integral x
                del part_uvec[ii]
                del part_xvec[ii]
                print("Current unfixed: ", xunfixed_num)
                # print("Current xlist: \n", xunfixed_list)
            else:
                print('else')
                #solved = False
                ii += 1

        print("Current unfixed: ", xunfixed_num)
        print("Current xlist: ", xunfixed_list)


        # If one agent get sum one, keep sum one!
        ii = 0
        while ii < bagents_unsatnum:
            if abs(part_slvec[ii]) < tol: # slack=0 means constraint active
            # if np.inner(part_Amat[ii], part_xvec) == 1:
                # Put it onto Aeqmat and beqvec (equality constraints)
                Aeqmat += [[]]
                Aeqmat[-1] = part_Amat[ii]
                print("Add to Aeqmat: ", part_Amat[ii])
                beqvec.append(part_bvec[ii])
                print("Add to beqvec: ", part_bvec[ii])
                # Remove it from part_Amat and part_bvec (inequality constraints)
                del part_Amat[ii]
                del part_bvec[ii]
                del part_slvec[ii]
                # Update the num of unsatisfied agents
                bagents_unsatnum -= 1
                print("The {0}-th row of part_Amat is moved to Aeqmat".format(ii))
            else:
                ii += 1

        print("bagents_unsatnum: {0}".format(bagents_unsatnum))

        # At this point, if all x are fractional
        if allfrac:
            # Eliminate (Supply) constraints and solve
            jj = bagents_unsatnum
            while jj < len(part_Amat):
                if sum(part_Amat[jj]) <= part_bvec[jj] + bk - 1 + tol:
                    del part_Amat[jj]
                    del part_bvec[jj]
                    print("Del constraint {0}".format(jj))
                else:
                    jj += 1

        # Solve
        # print("part_Amat:", part_Amat)
        # print("part_bvec:", part_bvec)
        # print("Aeqmat:", Aeqmat)
        # print("beqvec:", beqvec)
        nres = opt.linprog(part_uvec, A_ub=part_Amat, b_ub=part_bvec, A_eq=Aeqmat, b_eq=beqvec)
        printsol(nres, xunfixed_list)
        part_xvec = list(nres.x)
        part_slvec = list(nres.slack)

        # Stick the solution part_xvec onto xvec
        for i in range(xunfixed_num):
            xvec[xunfixed_list[i]] = part_xvec[i]

    # end while 1
    print("\nFinal solution:")
    for i in range(bagents * bundle_list_size):
        if xvec[i] > tol:
            print("Agent {0} is allocated {1:.3f} portion of bundle: {2}".format(i // bundle_list_size, xvec[i], bundle_list[i % bundle_list_size]))

    print("Dual solution (Market Clearing Price):")
    for j in range(bgoods):
       print("Price for good {0}: {1:.2f}".format(j, pvec[j]))

    checksol(xvec, pvec, uvec)
    return xvec, pvec
# end function

def printsol(res, xlist=list(range(bagents * bundle_list_size)), tol=1e-12):
    """Print solution of LP"""
    # Print
    print("\nLinprog exit with code {0}".format(res.status))
    # print("Current xlist: {0}".format(xlist))
    for i in range(len(xlist)):
        ind = xlist[i]
        if res.x[i] > tol:
            print("Agent {0} is allocated {1:.3f} portion of bundle: {2}".format(ind // bundle_list_size, res.x[i], bundle_list[ind % bundle_list_size]))
    print("Slack: ", res.slack)
    print("Value: ", res.fun)

def checksol(xvec, pvec, uvec):
    """Check whether the solution is reasonable"""
    print("### Checksol")
    tol = 1e-5
    for i in range(bagents):
        alphai = 0
        argmax_alphai = {-1}
        ichoice = -1
        for k in range(bundle_list_size):
            b = bundle_list[k]
            payoff = -uvec[i * bundle_list_size + k] - sum([b.count(j) * pvec[j] for j in range(bgoods)])
            #print("Agent {0} bundle {1} payoff: {2}".format(i, b, payoff))
            if payoff > alphai + tol:
                argmax_alphai = {k}
                alphai = payoff
            elif abs(payoff - alphai) <= tol:
                argmax_alphai.add(k)
                alphai = payoff
            if xvec[i * bundle_list_size + k] > 1 - tol:
                ichoice = k
        b = bundle_list[ichoice] if ichoice >= 0 else ()
        if ichoice in argmax_alphai:
            print("Agent {agent} acts rationally and buys {bundle}".format(agent=i, bundle=b))
        else:
            bset = [bundle_list[jj] for jj in argmax_alphai]
            print("Agent {agent} acts IRRATIONALLY and buys {bundle}, she could better choose among bundle {bset}".format(agent=i, bundle=b, bset=bset))

if __name__ == '__main__':
    bacalc()
