Spectrum Allocation Tool v0.1
=======
Author: Dengwang Tang
-----------

The tool is designed to run POPT mechanism to get an approximately efficient envy-free allocation of bands on an rectangular arena.

How to run the tool on linux
-----------

1. Install python3.4
2. Make sure you have the following packages: cvxpy, cvxopt, ecos, tkinter

	(Usually tkinter would be automatically included, and when you install cvxpy, cvxopt and ecos would be automatically installed.)

3. python bagui.py

We recommend you to use Anaconda3 for python.

Section I. Input
-----------
The tool accepts two modes of input: json and plain text.

Before reading the next part:

* All the coordinates in the following document starts at zero. 
* The rows are counted from above (or north) to below (or south), and columns are counted from left (or west) to right (or east).


### JSON input:

For basic format for a JSON file, see http://json.org/

Required Field: `grid`, `k`, `bj`, `utility`:

* `grid`: Required. Number(s) representing the size of the arena. A list with two integers: \[length, width\] if the arena is 2-dimentional, or a list of one integer \[length\] if the arena is 1-dimentional.

* `k`: Required. A positive integer representing complementarity (the maximum number of bands an agent can get)

* `bj`: Required. Number of bands in each block of the arena. Could be a positive integer. Could be a matrix of positive integers with the same shape as the arena

* `utility`: Required. Could be

	1. A dictionary which maps the names of the agents to the utility matrices (or utility sparse matrices). 
	
	2. A list of utility matrices (or utility sparse matrices) (In this case the names of the agents are automatically assigned)
	
		The utility matrix should have the same shape as the arena, each entry represents the base utility of the band in corresponding block.
	
		The utility sparse matrix should be a dictionary which maps the coordinate(s) to utility values. The coordinates are written in form "(A,B)"(2-dimentional arena) or "(A)" (1-dimentional arena), where A, B can be either a number j or a range j:k. The utility values can be either one number or a matrix with the same shape as the coordinate(s). The utility for the unspecified coordinates are set to zero by default.

* `cost`: Optional. Representing the boundary costs of the agents. Could be

	1. A dictionary which maps the names of the agents to the cost sparse matrices

	2. A list of cost sparse matrices

		The above type must conform with the type choice of "utility" field.

		The keys of the cost sparse matrix are coordinate(s) with a direction specifier in the end, for example: "(0:2,1)E". For 2-dimentional arenas, the directions can be \[NESW\]. For 1-dimentional arenas, the directions can be \[LR\]. The values are cost values which are either a numerical value or a matrix with the same shape as the coordinate(s). The costs unspecified boundaries are set to be zero by default.

		If the direction is pointing outwards the arena (pointing to an excluded coordinate is not considered as outwards,) for example "(0,1)N", then this entry is omitted.

		If an agent is missing, then the costs for the agent are all set to zero by default.

* `exclude`: Optional. list of coordinates to be excluded. If the arena is 2-dimensioinal, then the coordinate is a list of two positive integers \[x, y\]. If the arena is 1-dimensional, then the coordinate is a positive integer.

		The excluded coordinates would be considered to have no band to allocate. The number of bands specified in "bj" would be overridden by "exclude"

### Plain text input

All files with extension other than .json are viewed as plain text file. Comments starts with hash sign are allowed at the end of each row

* first row: length, width, k, bj (k and bj have the same meaning as above.) All positive integers
* second row: A blank row (except commments)
* next a few rows: a matrix representing the utility of the goods to each agent. The utility of block (b1, b2) for the i-th agent is contained in the i-th row, (b1*width+b2)-th column
* next: A blank row (except comments)
* next a few rows: a matrix representing the cost of the boundaries to each agent. The i-th row specifies the cost of the i-th agent. The cost are listed in the following order:
	
	1. Lexicographical order of coordinates
	2. "N", "E", "S", "W" (2-dimentional), or "L", "R" (1-dimentional)
	3. outwards boundaries (pointing to an excluded coordinate is not considered as outwards) are not included

#### Example:
```
	(0, 0)E
	(0, 0)S
	(0, 1)E
	(0, 1)S
	(0, 1)W
	(0, 1)E
	(0, 1)S
	(0, 1)W
	...
	(1, 1)N
	(1, 1)E
	(1, 1)S
	(1, 1)W
	(1, 2)N
	(1, 2)E
	(1, 2)S
	(1, 2)W
	...
```
An example of json data input
```
{
	"grid": [3, 3],
	"k": 3,
	"bj": 10,
	"exclude": [
		[0, 2],
		[1, 2]
	],
	"utility": {
		"Alice": [
			[1,2,3],
			[4,5,6],
			[7,8,9]
		],
		"Bob": [
			[9,8,7],
			[6,5,4],
			[3,2,1]
		],
		"Eve": {
			"(0,1:2)": [1, 1.1],
			"(1,2)": 2
		}
	},
	"cost": {
		"Alice": {
			"(0,1)N": 1,
			"(1:2,1:2)E": [
				[1.4, 1.4], 
				[1.3, 1.3]
			]
		}
	}
}
```

Another example of json data input:
```
{
	"grid": [5],
	"k": 3,
	"bj": [3,4,5,6,7],
	"utility": [
		[
			[1,2,3,4,5]
		],
		[
			[9,8,7,6,5]
		],
		{
			"(0)": 1,
			"(2)": 2
		}
	],
	"cost": [
		{
			"(0)R": 1,
		},
		{
			"(1:2)L": 1,
		},
		{}
	]
}
```

An example of plain data input:
```
3 3 3 10
	# utility
1 2 3 4 5 6 7 8 9
9 8 7 6 5 4 3 2 1
5 5 5 5 5 5 5 5 5
4 6 4 6 4 6 4 6 4
6 4 6 4 6 4 6 4 6
	# cost
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5 0.5
```

### Use genprofile.py to generate a random profile
Currently the random profile creation is not supported through GUI tool. However, you can also use the genprofile.py script to generate a random utility profile.

Usage: `python genprofile.py l w k outputfile`

* The resulting arena will have size `l` times `w`
* A plain data input is generated and save in a file named `outputfile`

After generating a profile. This profile can be loaded into the GUI tool through a file openning dialog.

Section II. Output
----------------------
After importing data from file. Click "Calculate" to get the allocation scheme. Since the calculation is complicated, it may take some time, especially when the arena is larger then 30 blocks or k > 3.

The output would be shown in a new window. The bands alloted to each agent is visualized as grids. Where the colors can be intepreted as:

* gray: The agent is given no band in this region
* green: The agent is given 1 band in this region
* orange: The agent is given 2 bands in this region
* magenta: The agent is given 3 bands in this region
* red: The agent is given 4 bands in this region
* purple: The agent is given more than 4 bands in this region
