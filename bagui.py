#!/usr/bin/env python

__author__ = "dwtang"

__all__ = ["bagui_main"]

import balloc as baapi

import ast
from itertools import combinations as cb
from itertools import combinations_with_replacement as cr
import json
import numpy as np
import re
import tkinter as tk
from tkinter import filedialog as tkfd
from tkinter import font as tkft
from tkinter import messagebox as tkmsg
import time, sys

class ErrorWindow(BaseException):
    """When an exception happens in processing requests, throw error of this type would generate an error messagebox."""
    def __init__(self, message):
        super(ErrorWindow, self).__init__()
        self.message = message
    def __str__(self):
        return self.message

class baApp(tk.Frame):
    """Band Allocation Application main window"""
    def __init__(self, master):
        super(baApp, self).__init__()
        self.version = '0.1'
        master.title("Spectrum Allocation Tool v" + self.version)
        # master.iconbitmap('apple_transparent.ico')
        master.resizable(height=False, width=False)
        self.import_window = None
        self.gen_window = None
        self.result_window = None
        self.pack()
        self.hasdata = False
        self.create_wids()

    def create_wids(self):
        self.statusbox = baStatusbox(self, height=280, width=300)
        self.statusbox.grid(row=0, column=1, rowspan=2, padx=10, pady=5)
        
        self.importbox = tk.LabelFrame(self, width=200, height=400, bd=3, relief="groove", text="profile", padx=5, pady=5)
        self.importbox.grid(row=0, column=0, padx=10, pady=5, sticky=tk.N+tk.S)

        self.importlabel = tk.Label(self.importbox, text="Import utility profile from existing file:")
        self.importlabel.grid(row=0, column=0, padx=5, pady=5, sticky=tk.NE)

        self.importbutton = tk.Button(self.importbox, text="Import")
        self.importbutton.grid(row=0, column=1, padx=5, pady=5, sticky=tk.NE)
        self.importbutton.bind("<Button-1>", self.import_from_file)

        self.genlabel = tk.Label(self.importbox, text="Start new utility profile creation process:")
        self.genlabel.grid(row=1, column=0, padx=5, pady=5, sticky=tk.NE)

        self.genbutton = tk.Button(self.importbox, text="Create")
        self.genbutton.grid(row=1, column=1, padx=5, pady=5, sticky=tk.NE)
        self.genbutton.bind("<Button-1>", self.create_new_profile)

        self.calcbox = tk.LabelFrame(self, width=200, height=400, bd=3, relief="groove", text="calculate", padx=5, pady=5)
        self.calcbox.grid(row=1, column=0, padx=10, pady=5, sticky=tk.N+tk.S+tk.E+tk.W)

        self.calclabel = tk.Label(self.calcbox, text="Calculate allocation and prices:")
        self.calclabel.grid(row=0, column=0, padx=5, pady=5, sticky=tk.NE)

        self.calcbutton = tk.Button(self.calcbox, text="Calculate!")
        self.calcbutton.grid(row=0, column=1, padx=5, pady=5, sticky=tk.NE)
        self.calcbutton.bind("<Button-1>", self.do_calc)

    def import_from_file(self, event):
        try:
            r = import_from_file_helper()
        except ErrorWindow as err:
            # raise err
            tkmsg.showerror('Data Import Error', err)
        else:
            if r == None:
                return
            (infilename, self.bagents, self.bnames, self.bgoods, self.blength, 
                self.bwidth, self.bgoodlist, self.adjlist, self.bjvec, 
                self.bk, self.umat, self.costmat) = r
            ststr = "Current Profile:\nImported data from " + infilename + '\n'
            ststr += "Number of agents: {0:d}\n".format(self.bagents)
            ststr += "Number of different goods: {0:d}\n".format(self.bgoods)
            ststr += "Number of complementarity: {0:d}\n".format(self.bk)
            self.statusbox.changestatus(ststr)
            print('self.bgoodlist: ', self.bgoodlist)
            print('self.umat: ', self.umat)
            # print('self.adjlist', self.adjlist)
            showmat = [[self.adjlist[i], [self.costmat[j][i] for j in range(len(self.costmat))]] for i in range(len(self.adjlist))]
            print('self.costmat:')
            for row in showmat:
                print(row)
            self.hasdata = True

    def create_new_profile(self, event):
        pass

    def do_calc(self, event):
        if not self.hasdata:
            tkmsg.showerror('No data', "No data available for calculation!")
            return

        self.bname2num = {self.bnames[i]: i for i in range(self.bagents)}
        self.bgood2num = {self.bgoodlist[i]: i for i in range(self.bgoods)}

        self.bundle_list = []
        for i in range(self.bk):
            self.bundle_list += list(cr(self.bgoodlist, i+1))
        self.bundle_list_size = len(self.bundle_list)
        # ugmat = np.array(self.umat)
        bpairs = len(self.adjlist)

        Bmat = np.zeros((self.bgoods, self.bundle_list_size), dtype='int')
        for b in range(self.bundle_list_size):
            for j in range(self.bgoods):
                Bmat[j, b] = self.bundle_list[b].count(self.bgoodlist[j])

        # cmat = np.zeros((self.bagents, bpairs), dtype='float32')
        dBmat = np.zeros((bpairs, self.bundle_list_size), dtype='int')
        for p in range(len(self.adjlist)):
            pairme = self.adjlist[p][0]
            if self.adjlist[p][1] == 'N':
                pairother = pairme - self.bwidth
            elif self.adjlist[p][1] == 'E':
                pairother = pairme + 1
            elif self.adjlist[p][1] == 'S':
                pairother = pairme + self.bwidth
            elif self.adjlist[p][1] == 'W':
                pairother = pairme - 1
            elif self.adjlist[p][1] == 'L':
                pairother = pairme - 1
            elif self.adjlist[p][1] == 'R':
                pairother = pairme + 1
            for b in range(self.bundle_list_size):
                if pairother in self.bgood2num:
                    dBmat[p, b] = Bmat[self.bgood2num[pairme], b] - Bmat[self.bgood2num[pairother], b]
                    if dBmat[p, b] < 0:
                        dBmat[p, b] = 0
                else:
                    dBmat[p, b] = Bmat[self.bgood2num[pairme], b]

        self.Umat = np.dot(self.umat, Bmat) - np.dot(self.costmat, dBmat)

        Onemat = [[0]*(i*self.bundle_list_size) + [1]*self.bundle_list_size + 
         [0]*(self.bagents-i-1)*self.bundle_list_size for i in range(self.bagents)]
        Twomat = [[b.count(j) for b in self.bundle_list]*self.bagents for j in self.bgoodlist]
        Amat = Onemat + Twomat
        bvec = [1]*self.bagents + list(self.bjvec)

        wdelta, pdelta, self.tol = 5e-5, 1e-2, 1e-5

        bap = {"bagents":self.bagents, "bgoods":self.bgoods,
                    "bundle_list_size":self.bundle_list_size,
                    "umat":self.Umat, 
                    "Amat":Amat, 
                    "bvec":bvec, 
                    "k":self.bk,
                    "bundle_list":self.bundle_list, 
                    "wdelta":wdelta, "pdelta":pdelta, "tol":self.tol,
                    "method":'ECOS'}

        # print(bap)
        # ss = json.dumps(bap, indent=4)
        # with open('bap.json', 'w') as f:
        #     f.write(ss)
        # return

        t0 = time.time()
        for chance in range(100):
            try:
                Xmat, la, xvec, pvec, uval = baapi.bacalc(**bap)
            except baapi.SolverError as err:
                print('Error: ', err)
                print('Run it again ...')
                continue
            else:
                break
        else:
            tkmsg.showerror("Calculation Error", 
                'All trial failed, unlucky of you\nYou can run calculation again.')
            return

        calctime = time.time() - t0
        self.xvec = xvec
        self.pvec = pvec
        self.pdict = {self.bgoodlist[i]:pvec[i] for i in range(self.bgoods)}
        self.uval = uval
        self.svec = np.dot(Twomat, self.xvec.astype(int))

        smat = np.dot(Twomat, Xmat)
        print('smat:', smat)

        self.pval = np.dot(self.pvec, self.svec)
        self.print_result(calctime)

    def print_result(self, calctime):
        self.agent_bundle = [-1] * self.bagents
        # CLI print
        print("\nFinal solution:")
        for i in range(self.bagents * self.bundle_list_size):
            if self.xvec[i] > 1 - self.tol:
                iag = i // self.bundle_list_size
                ibd = i % self.bundle_list_size
                self.agent_bundle[iag] = ibd
                print("{0} is allocated {1:.3f} portion of bundle: {2}".format(
                    self.bnames[iag], self.xvec[i], self.bundle_list[ibd]))
        for g in self.bgoodlist:
            print('Price for good {0} is {1:.3f}'.format(g, self.pdict[g]))
        print("Total Utility: ", self.uval)
        # update status
        self.statusbox.updatestatus("\n\nCalculation Succeeds in {t:.2f} seconds.".format(t=calctime))
        # GUI print
        hh = 55 * self.blength + 255 if self.blength <= 20 else round(1100/self.blength) * self.blength + 205 
        if self.bwidth is not None and self.bwidth > 3:
            ww = 55 * self.bwidth + 35 if self.bwidth <= 20 else round(1100/self.bwidth) * self.bwidth + 35
        else:
            ww = 55*3+35
        self.resultwindow = baResultWindow(master=self, hh=hh, ww=ww)

class baResultWindow(tk.Toplevel):
    def __init__(self, master, hh, ww):
        super(baResultWindow, self).__init__(master, height=hh, width=ww)
        self.title('Allocation Result')
        self.resizable(height=False, width=False)
        self.agent_bundle = master.agent_bundle
        self.pdict = master.pdict
        self.pvec = master.pvec
        self.uval = master.uval
        self.svec = master.svec
        self.bjvec = master.bjvec
        self.pval = master.pval
        self.bundle_list = master.bundle_list
        self.bgoods = master.bgoods
        self.bgoodlist = master.bgoodlist
        self.blength = master.blength
        self.bwidth = master.bwidth
        self.bnames = master.bnames
        self.bname2num = master.bname2num
        self.Umat = master.Umat
        self.create_wids()

    def create_wids(self):
        self.choosebox = tk.Frame(self)
        self.choosebox.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W+tk.E+tk.N+tk.S)

        self.cblabel = tk.Label(self.choosebox, text="Choose an agent to show")
        self.cblabel.grid(row=0, column=0, padx=5, sticky=tk.W)

        self.cbvar = tk.StringVar(self.choosebox)
        self.cboptions = tk.OptionMenu(self.choosebox, self.cbvar, *self.bnames)
        self.cboptions.grid(row=0, column=1, padx=5, sticky=tk.W)

        self.cbok = tk.Button(self.choosebox, text="OK", command=self.select_name)
        self.cbok.grid(row=0, column=2, padx=5, sticky=tk.W)

        self.sslabel = tk.Label(self.choosebox, text="Or show global statistics")
        self.sslabel.grid(row=1, column=0, padx=5, sticky=tk.W)

        self.ssok = tk.Button(self.choosebox, text="Show statistics", command=self.show_stats)
        self.ssok.grid(row=1, column=1, padx=5, sticky=tk.W)

        hh = 55 * self.blength + 205 if self.blength <= 20 else round(1100/self.blength) * self.blength + 155 
        if self.bwidth is not None and self.bwidth > 3:
            ww = 55 * self.bwidth + 35 if self.bwidth <= 20 else round(1100/self.bwidth) * self.bwidth + 35
        else:
            ww = 55*3+35

        self.resultframe = tk.LabelFrame(self, bd=3, relief='groove', 
            text='resulte', height=hh, width=ww)
        self.resultframe.grid(row=2, column=0, padx=5, pady=5, sticky=tk.W+tk.E+tk.N+tk.S)
        self.resultframe.grid_propagate(0)

    def show_stats(self):
        self.resultframe = tk.LabelFrame(self, bd=3, relief='groove', text='result')
        self.resultframe.grid(row=2, column=0, padx=5, pady=5, sticky=tk.W+tk.E+tk.N+tk.S)
        self.resultframe.grid_propagate(0)
        self.rfstr = "Total allocated goods: {0:.0f}\n".format(sum(self.svec))
        self.rfstr += "Total utility: {0:.2f}\n".format(self.uval)
        self.rfstr += "Total price: {0:.2f}".format(self.pval)
        self.rflb = tk.Label(self.resultframe, text=self.rfstr, justify=tk.LEFT)
        self.rflb.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W)

        self.rfstr_altnum = "Actual allot number of bands in each region: "
        self.rfstr_altnum_lb = tk.Label(self.resultframe, text=self.rfstr_altnum)
        self.rfstr_altnum_lb.grid(row=1, column=0, padx=5, sticky=tk.W)

        self.rfstr_price = "Price of a band in each region: "
        self.rfstr_price_lb = tk.Label(self.resultframe, text=self.rfstr_price)
        self.rfstr_price_lb.grid(row=3, column=0, padx=5, sticky=tk.W)

        self.rfstr_altnum_show = tk.Button(self.resultframe, text="Show", command=self.show_number)
        self.rfstr_altnum_show.grid(row=2, column=0, padx=5, sticky=tk.E)

        self.rfstr_price_show = tk.Button(self.resultframe, text="Show", command=self.show_price)
        self.rfstr_price_show.grid(row=4, column=0, padx=5, sticky=tk.E)

        self.hh = 55 * self.blength + 5 if self.blength <= 20 else round(1100/self.blength) * self.blength + 5 
        if self.bwidth is not None and self.bwidth > 3:
            self.ww = 55 * self.bwidth + 5 if self.bwidth <= 20 else round(1100/self.bwidth) * self.bwidth + 5
        else:
            self.ww = 55*3+5

    def show_number(self):
        self.rfarena = tk.Canvas(self.resultframe, height=self.hh, width=self.ww)
        self.rfarena.grid(row=5, column=0, sticky=tk.N+tk.S)
        self.rfarena.grid_propagate(0)

        myfont = tkft.Font(family='Helvetica', size=16)
        
        colors = ['cyan', 'green', 'orange', 'magenta', 'red', 'purple']
        for i in range(self.bgoods):
            if self.bwidth == None:
                ix, iy = self.bgoodlist[i], 0
            else:
                ix = self.bgoodlist[i] // self.bwidth
                iy = self.bgoodlist[i] % self.bwidth
            
            bands = self.svec[i]
            bandstxt = "{0:.0f}".format(bands)
            exbands = bands - self.bjvec[i]
            if exbands <= 0:
                color = colors[0]
            elif exbands < 5:
                color = colors[exbands]
            else:
                color = colors[-1]

            self.rfarena.create_rectangle(5+iy*55, 5+ix*55, 56+iy*55, 56+ix*55, fill=color)
            self.rfarena.create_text(30+iy*55, 30+ix*55, text=bandstxt, font=myfont)

    def show_price(self):
        self.rfarena = tk.Canvas(self.resultframe, height=self.hh, width=self.ww)
        self.rfarena.grid(row=5, column=0, sticky=tk.N+tk.S)
        self.rfarena.grid_propagate(0)

        myfont = tkft.Font(family='Helvetica', size=12)
        
        # colors = ['cyan', 'green', 'orange', 'magenta', 'red', 'purple']
        for i in range(self.bgoods):
            if self.bwidth == None:
                ix, iy = self.bgoodlist[i], 0
            else:
                ix = self.bgoodlist[i] // self.bwidth
                iy = self.bgoodlist[i] % self.bwidth
            
            price = self.pvec[i]
            pricetxt = "{0:.2f}".format(price)

            self.rfarena.create_rectangle(5+iy*55, 5+ix*55, 56+iy*55, 56+ix*55, fill="orange")
            self.rfarena.create_text(30+iy*55, 30+ix*55, text=pricetxt, font=myfont)

    def select_name(self):
        currentname = self.cbvar.get()
        currenti = self.bname2num[currentname]
        self.resultframe = ResultFrame(self, name=currentname, namei=currenti)
        self.resultframe.grid(row=2, column=0, padx=5, sticky=tk.W+tk.E+tk.N+tk.S)

class ResultFrame(tk.LabelFrame):
    def __init__(self, master, name, namei):
        print('Frame!')
        super(ResultFrame, self).__init__(master, bd=3, relief='groove', 
            text='result')
        self.grid_propagate(0)
        self.wordstr = "Agent {name} gets the following bands:\n".format(name=name)
        if master.agent_bundle[namei] > -1:
            bd = master.bundle_list[master.agent_bundle[namei]]
            uu = master.Umat[namei, master.agent_bundle[namei]]
        else:
            bd = ()
            uu = 0.00
        
        tp = 0.00
        for g in sorted(set(bd)):
            self.wordstr += "{1} band in region #{0}\n".format(g, bd.count(g))
            try:
                tp += master.pdict[g] * bd.count(g)
            except KeyError as err:
                print('bd:', bd)
                print('g:', g)
                print(master.pdict)
                print(err)
                sys.exit()
        self.wordstr += "Total Utility: {0:.2f}\n".format(uu)
        self.wordstr += "Total Price: {0:.2f}\n".format(tp)
        self.word = tk.Label(self, text=self.wordstr, justify=tk.LEFT)
        self.word.grid(row=0, column=0, padx=5, pady=5, sticky=tk.W+tk.N+tk.S)

        hh = 55 * master.blength + 5 if master.blength <= 20 else round(1100/master.blength) * master.blength + 5 
        if master.bwidth is not None and master.bwidth > 3:
            ww = 55 * master.bwidth + 5 if master.bwidth <= 20 else round(1100/master.bwidth) * master.bwidth + 5
        else:
            ww = 55*3+5

        self.arena = tk.Canvas(self, height=hh, width=ww)
        self.arena.grid(row=1, column=0, sticky=tk.N+tk.S)
        self.arena.grid_propagate(0)
        
        colors = ['gray', 'green', 'orange', 'magenta', 'red', 'purple']
        for i in master.bgoodlist:
            if master.bwidth == None:
                ix, iy = i, 0
            else:
                ix = i // master.bwidth
                iy = i % master.bwidth
            gcount = bd.count(i)
            if gcount < 5:
                color = colors[gcount]
            else:
                color = colors[5]

            self.arena.create_rectangle(5+iy*55, 5+ix*55, 56+iy*55, 56+ix*55, fill=color)

class baStatusbox(tk.LabelFrame):
    def __init__(self, master, height, width):
        super(baStatusbox, self).__init__(master, bd=3, relief="groove", 
            text="status", height=height, width=width)
        self.grid_propagate(0)
        self.str = tk.StringVar()
        self.str.set("No data")
        self.showtext()

    def showtext(self):
        self.label = tk.Label(self, textvariable=self.str, justify=tk.LEFT, wraplength=240)
        self.label.grid(row=0, column=0, padx=10, pady=5, sticky=tk.N+tk.S)

    def changestatus(self, text):
        self.str.set(text)

    def updatestatus(self, text):
        s = self.str.get()
        s += text
        self.str.set(s)

def readrange(s, ub):
    if s.isnumeric():
        k = int(s)
        if k >= ub:
            raise ErrorWindow('Invalid coordinate')
        return range(k, k+1)
    ss = s.split(sep=':')
    a, b = int(ss[0]), int(ss[1])
    if a >= b:
        raise ErrorWindow('Invalid range')
    if b >= ub:
        raise ErrorWindow('Invalid range')
    return range(a, b+1)

def import_from_file_helper():
    infilename = tkfd.askopenfilename(
        title='Select Utility Profile',
        filetypes=[("Javascript Object Notation File", "*.json"),
        ("Plain File", "*.txt"), 
        ("All Files", "*")])
    
    if infilename == '':
        print('Cancelled')
        return None
    filepart = infilename.split(sep='.')
    fext = filepart[-1]
    if fext == 'json':
        try:
            with open(infilename, 'r') as f:
                try:
                    data = json.load(f)
                except ValueError:
                    raise ErrorWindow('Invalid JSON')
        except IOError:
            raise ErrorWindow('Unable to open file')

        # Exist grid, bj, k, utility
        if not {"grid", "bj", "k", "utility"} <= data.keys():
            raise ErrorWindow("Missing important data in file")

        # Grid should be valid
        try:
            grid = np.array(data["grid"], dtype='int')
        except ValueError:
            raise ErrorWindow("Unrecognized grid")
        dim = grid.shape
        gdim = dim[0]
        if len(dim) != 1 or gdim > 2:
            raise ErrorWindow("Unsupported grid dimension")
        if not np.all(grid > 0):
            raise ErrorWindow("Grid parameters should be positive integers")
        
        blength, bwidth = None, None
        if gdim == 2:
            blength, bwidth = grid[0], grid[1]
            bgoodset = set(range(blength * bwidth))
        else:
            blength = grid[0]
            bgoodset = set(range(grid[0]))

        # handle exclude list
        if "exclude" in data:
            try:
                excludelist = np.array(data["exclude"], dtype='int')
            except ValueError:
                raise ErrorWindow('exclude list not valid')
            mn = excludelist.shape
            if gdim == 1 and len(mn) != 1:
                raise ErrorWindow('exclude list not valid')
            elif gdim == 2 and (len(mn) != 2 or mn[1] != 2):
                raise ErrorWindow('exclude list not valid')
            if not np.all(excludelist >= 0):
                raise ErrorWindow('exclude list not valid')
            if gdim == 1:
                if not np.all(excludelist < grid[0]):
                    raise ErrorWindow('excludelist is not valid')
            else:
                if not np.all(excludelist[:,0] < grid[0]):
                    raise ErrorWindow('excludelist is not valid')
                if not np.all(excludelist[:,1] < grid[1]):
                    raise ErrorWindow('excludelist is not valid')
            for coord in excludelist:
                if gdim == 1:
                    bgoodset.discard(coord)
                elif gdim == 2:
                    bgoodset.discard(coord[0] * bwidth + coord[1])

        bgoodlist = sorted(bgoodset)
        bgoods = len(bgoodlist)
        # bk should be valid (appropriate data structure, positive)
        try:
            bk = int(data["k"])
        except TypeError:
            raise ErrorWindow("Unrecognized k")
        if bk <= 0:
            raise ErrorWindow("k should be a positive integer")

        # bj should be valid (appropriate data structure, non-negative)
        try:
            bjmat = np.array(data["bj"], dtype='int')
        except:
            raise ErrorWindow("Unrecognized bj")
        shpbj = bjmat.shape
        if shpbj == ():
            if int(data["bj"]) < 0:
                raise ErrorWindow("Individual bj should be positive")
            bjvec = np.array([int(data["bj"])] * bgoods, dtype='int')
        elif shpbj == tuple(grid):
            if not np.all(bjmat >= 0):
                raise ErrorWindow("bj should be non-negative")
            bjvec = np.ravel(bjmat)
            bjvec = np.take(bjvec, bgoodlist)
        else:
            raise ErrorWindow("bj size is not valid")


        # bgoods, bjvec, bk, 
        # utility: two modes: list and dict
        if isinstance(data["utility"], list):
            listmode = True
            umats = data["utility"]
            bagents = len(umats)
            if bagents == 0:
                raise ErrorWindow("Empty utility, not allowed!")
            bnames = ["Agent_" + str(i) for i in range(1, bagents+1)]
        elif isinstance(data["utility"], dict):
            listmode = False
            bnames = list(data["utility"].keys())
            bagents = len(bnames)
            if bagents == 0:
                raise ErrorWindow("Empty utility, not allowed!")
            bnames.sort()
            umats = [data["utility"][s] for s in bnames]
        else:
            raise ErrorWindow("utility not valid")

        # bagents, bgoods, bjvec, bk, missing: utility data
        bagents = len(bnames)
        sregex = '\((\d+|\d+:\d),(\d+|\d+:\d)\)' if gdim == 2 else '\((\d+|\d+:\d)\)'
        
        ubmat = []
        for item in umats:
            if isinstance(item, list):
                try:
                    umat = np.array(item, dtype='float32')
                except ValueError:
                    raise ErrorWindow("utility not valid")
                if umat.shape != tuple(grid):
                    raise ErrorWindow("utility matrix not valid")
                ubmat.append(np.ravel(item))
            elif isinstance(item, dict):
                umat = np.zeros(grid)
                for key in item:
                    try:
                        um = np.array(item[key], dtype='float32')
                    except ValueError:
                        raise ErrorWindow('utility sparse matrix not valid!')
                    m = re.match(sregex, key, re.I)
                    if m is None:
                        raise ErrorWindow("utility sparse matrix not valid")
                    skey = key[1:-1]
                    if gdim == 1:
                        cordlist = readrange(s, grid[0])
                        # for ind in cordlist:
                        #     umat[ind] = item[key]
                        if um.shape != () and um.shape != (len(cordlist),):
                            raise ErrorWindow('Size Error')
                        umat[cordlist] = um
                    else:
                        ss = skey.split(sep=',')
                        p0 = readrange(ss[0], grid[0])
                        p1 = readrange(ss[1], grid[1])
                        if um.shape != ():
                            if len(p0) != 1 and len(p1) != 1:
                                if um.shape != (len(p0), len(p1)):
                                    raise ErrorWindow('Size Error')
                            elif len(p0) == 1:
                                # print(um.shape)
                                # print(p0, p1)
                                if um.shape != (1, len(p1)) and um.shape != (len(p1),):
                                    raise ErrorWindow('Size Error')
                            elif len(p1) == 1:
                                if um.shape != (len(p0), 1) and um.shape != (len(p0),):
                                    raise ErrorWindow('Size Error')
                        # cordlist = [(a,b) for a in p0 for b in p1]
                        # for ind in cordlist:
                        #     umat[ind] = item[key]
                        umat[p0, p1] = um
                ubmat.append(np.ravel(umat))
        ubmat = np.array(ubmat, dtype='float32')
        ubmat = np.take(ubmat, bgoodlist, axis=1)
        # bagents, bgoods, bjvec, bk, ubmat, missing: cmat
        # make adjlist
        adjlist = []
        if gdim == 2:
            for l in bgoodlist:
                if l % bwidth != 0:
                    adjlist.append((l, 'W'))
                if l % bwidth != bwidth - 1:
                    adjlist.append((l, 'E'))
                if l // bwidth != 0:
                    adjlist.append((l, 'N'))
                if l // bwidth != blength - 1:
                    adjlist.append((l, 'S'))
        else:
            for l in bgoodlist:
                if l != 0:
                    adjlist.append((l, 'L'))
                if l != blength - 1:
                    adjlist.append((l, 'R'))

        adjlistlen = len(adjlist)
        # if cost not specified, cost is 0
        if "cost" not in data:
            costmat = [[0] * adjlistlen] * bagents
        # else cost is given by two modes: list or dict
        else:
            if isinstance(data["cost"], list):
                if not listmode:
                    raise ErrorWindow('Utility incompatible with cost')
                if len(data["cost"]) != len(data["utility"]):
                    raise ErrorWindow('Utility incompatible with cost')
                cmats = data["cost"]
            elif isinstance(data["cost"], dict):
                if listmode:
                    raise ErrorWindow('Utility incompatible with cost')
                if not data["cost"].keys() <= data["utility"].keys():
                    raise ErrorWindow('Agent name incompatible')
                cmats = [data["cost"][k] if k in data["cost"] else {} for k in bnames]

            if gdim == 2:
                costregex = '\((\d+|\d+:\d+),(\d+|\d+:\d+)\)[NESW]'
            else:
                costregex = '\((\d+|\d+:\d+)\)[LR]'

            costmat = []
            for item in cmats:
                costdict = {}
                try:
                    np.array(list(item.values()), dtype='float32')
                except:
                    raise ErrorWindow('invalid cost')
                for key in item:
                    if not re.match(costregex, key, re.I):
                        raise ErrorWindow('invalid key for cost')
                    if gdim == 1:
                        cordlist = readrange(key[1:-2], blength)
                        for ind in cordlist:
                            costdict[(ind, key[-1])] = item[key]
                    elif gdim == 2:
                        ss = key[1:-2].split(sep=',')
                        p0 = readrange(ss[0], blength)
                        p1 = readrange(ss[1], bwidth)
                        cordlist = [ a*grid[1]+b for a in p0 for b in p1]
                        for ind in cordlist:
                            costdict[(ind, key[-1])] = item[key]
                costvec = [costdict[k] if k in costdict else 0 for k in adjlist]
                costmat.append(costvec)

    else: # txt file mode
        try:
            with open(infilename, 'r') as f:
                line = f.readline()
                line = line.split(sep='#')[0]
                line = line.strip()
                line = line.split()
                if len(line) < 4:
                    raise ErrorWindow('Not enough input arguments!')
                try:
                    blength, bwidth, bk, bj = int(line[0]), int(line[1]), int(line[2]), int(line[3])
                except ValueError:
                    raise ErrorWindow('Invalid blength, bwidth, or bk!')
                if blength <= 0 or bwidth <= 0 or bk <= 0 or bj <= 0:
                    raise ErrorWindow('Non-positive blength, bwidth, or bk!')

                f.readline()
                ubmat = []
                bagents = 0
                for line in f:
                    line = line.split(sep='#')[0]
                    line = line.strip()
                    if line == '':
                        break
                    try:
                        ubmat += [[float(w) for w in line.split()]]
                    except ValueError:
                        raise ErrorWindow('Invalid utility!')
                    bagents += 1

                # f.readline()

                bnames = ["Agent_" + str(i) for i in range(bagents)]
                bgoods = blength * bwidth
                bgoodlist = list(range(bgoods))
                bjvec = [bj] * bgoods
                adjlist = []
                for l in bgoodlist:
                    if l // bwidth != 0:
                        adjlist.append((l, 'N'))
                    if l % bwidth != bwidth - 1:
                        adjlist.append((l, 'E'))
                    if l // bwidth != blength - 1:
                        adjlist.append((l, 'S'))
                    if l % bwidth != 0:
                        adjlist.append((l, 'W'))
                costmat = np.zeros((bagents, len(adjlist)))
                costcount = 0
                print('bagents: ', bagents)
                for line in f:
                    line = line.split(sep='#')[0]
                    line = line.strip()
                    try:
                        # print(len(costmat[costcount]), len(line.split()))
                        costmat[costcount, :] = [float(w) for w in line.split()]
                        print("costcount: ", costcount)
                        # pass
                    except ValueError:
                        raise ErrorWindow('Invalid cost!')
                    costcount += 1
                    if costcount > bagents:
                        raise ErrorWindow('Agent number not match')
        except IOError:
            raise ErrorWindow('Unable to open file!')
        # raise ErrorWindow('Currently not supported') # update soon

    return infilename, bagents, bnames, bgoods, blength, bwidth, bgoodlist, adjlist, bjvec, bk, ubmat, costmat

def bagui_main():
    """Main function for bagui"""
    root = tk.Tk()
    app = baApp(root)
    root.mainloop()

if __name__ == '__main__':
    bagui_main()