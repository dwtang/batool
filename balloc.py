__author__ = "dwtang"

__all__ = ["SolverError", "bacalc"]

import numpy as np
import numpy.matlib as npm
# import cvxpy as cvx
from cvxpy import Maximize, Minimize, Problem, quad_form, Variable
from scipy.optimize import linprog 

import time, sys

class SolverError(BaseException):
    """docstring for SolverError"""
    def __init__(self, value):
        super(SolverError, self).__init__()
        self.value = value
    def __str__(self):
        return repr(self.value)

def bacalc(**bap):
    """Calculate out an allocation with data given in bap.
        bap: {'bagents', 'bgoods', 'bundle_list_size',
         'umat', 'Amat', 'bvec', 'k'}
    """
    tol = bap["tol"]
    stol = tol * 1e-2
    # CVXOPT options
    # solvers.options["show_progress"] = False
    # solvers.options["abstol"] = stol

    bagents = bap["bagents"]
    bgoods = bap["bgoods"]
    k = bap["k"]

    bbundles = bap["bundle_list_size"]
    bvars = bap["bagents"] * bap["bundle_list_size"]
    Amat = np.array(bap["Amat"], dtype='int')
    bvec = np.array(bap["bvec"], dtype='int')

    wdelta = bap["wdelta"]
    # weights = np.array([[1 - wdelta + 2 * wdelta * np.random.rand()] for i in range(bagents)])
    # wmat = npm.repmat(weights, 1, bbundles)
    wvec = 1 - wdelta + 2 * wdelta * np.random.rand(bvars)
    uvec = np.ravel(bap["umat"])
    # wuvec = np.ravel(np.multiply(bap["umat"], wmat))
    wuvec = np.multiply(uvec, wvec)

    pdelta = bap["pdelta"]
    xdelta = pdelta / np.linalg.norm(Amat[-1])
    pdata = [pdelta + pdelta * np.random.rand() for i in range(bgoods)]
    dbvec = np.append(np.zeros(bagents), pdata)
    pbvec = bvec - dbvec

    # wuvec = uvec
    
    print("Start Linprog")
    if bap["method"] == 'scipy':
        # ------------ scipy.optimize.linprog ------------
        pvec = [[]]
        def calcdual(xk, **kwargs):
            if kwargs["complete"]:
                bst = pbvec.size
                T = kwargs["tableau"]
                invB = T[:bst, bvars:bvars + bst]   # Inverse matrix
                bvvec = np.append(wuvec, [0] * bst)    # [uvec 0] is the bound vector for dual variables
                cbvec = []  # utility row vector corresponding to final basic variables 
                # Align the utility value according to the order of basic variables in column
                for bv in kwargs["basis"]:
                    for i in range(bst):
                        if T[i, bv] > 1 - tol:
                            cbvec.append(bvvec[bv]) # negate it back, here we use positive utility
                            break
                dvec = np.dot(invB.T, cbvec) # dual vec = c^B * B^(-1) = (B^-1)^T * c^B
                pvec[0] = dvec[bagents:] # price subvec of dual variable vec
                # Calculate the dual objfunc value, compare it with primal objfunc value
                # dualval = np.inner(pbvec, dvec)

        t0 = time.time()
        res = linprog(-wuvec, A_ub=Amat, b_ub=pbvec, callback=calcdual, options={"tol": stol})
        dt = time.time() - t0
        if not res["success"]:
            raise SolverError("Initial Linprog")
        print('\nUse time: {0:.2f} sec\n'.format(dt))
        xstar = res.x
        sstar = res.slack
        pstar = pvec[0]

    elif bap["method"] == 'ECOS':
        # ------------- CVXPY ----------------

        varx = Variable(bvars)
        ba_obj = Maximize(wuvec.T*varx)
        ba_cst = [Amat*varx <= pbvec, varx >= 0]
        ba_prob = Problem(ba_obj, ba_cst)

        t0 = time.time()
        ba_prob.solve(solver='ECOS', abstol=tol)
            # ['ECOS_BB', 'ECOS', 'CVXOPT', 'SCS', 'GLPK_MI', 'GLPK']
        dt = time.time() - t0
        # print('ba_prob.status: ', ba_prob.status)
        if ba_prob.status != 'optimal':
            raise SolverError("Initial Linprog: " + ba_prob.status)
        print('\nUse time: {0:.2f} sec\n'.format(dt))
        xstar = np.ravel(varx.value)
        sstar = pbvec - np.dot(Amat, xstar)
        pstar = np.ravel(ba_cst[0].dual_value)[bagents:]
    
        """
    elif bap["method"] == 'CVXOPT':
        # ------------ CVXOPT --------------

        cvec = matrix(-wuvec, tc='d');
        Imat = spmatrix(-1.0, range(bvars), range(bvars))
        Gmat = sparse([matrix(Amat), Imat])
        hvec = matrix(np.append(bap["bvec"]-dbvec, np.zeros(bvars)), tc='d')

        firstsol = conelp(cvec, Gmat, hvec)
        if firstsol['status'] != 'optimal':
            raise SolverError("Initial Linprog " + firstsol["status"])

        xstar = np.ravel(firstsol["x"])
        sstar = np.ravel(firstsol["s"])
        pstar = np.ravel(firstsol["z"])[bagents:bagents+bgoods]
        """

    else:
        raise NameError('Unknown Solver')
    # --------------------------------------------------
    
    printsol(xstar, bvars, bap["bundle_list_size"], bap["bundle_list"], tol)
    # sys.exit()

    for xib in xstar:
        if np.abs(xib - np.round(xib)) > tol:
            break
    else:
        Xmat = np.array([xstar]).T
        la = np.array([1.0])
        xvec = np.round(xstar)
        uval = np.dot(uvec, xvec)
        return Xmat, la, xvec, pstar, uval

    ieqlist = []
    for ii in range(bagents):
        if np.abs(sstar[ii]) <= tol:
            ieqlist.append(ii)

    Aeqmat = np.take(Amat, ieqlist, axis=0)
    beqvec = np.take(bvec, ieqlist)
    Amat = np.delete(Amat, ieqlist, axis=0)
    bvec = np.delete(bvec, ieqlist)
    bagents_unsatnum = bagents - len(ieqlist)

    # Lottery construction
    epsilon = np.sqrt(bvars) * tol
    print('epsilon: ', epsilon)
    conds = {"Amat": Amat, "Aeqmat": Aeqmat, "bvec": bvec, "beqvec": beqvec,
    "bagents_unsatnum": bagents_unsatnum, "k": k, "tol": tol}
    intxvec0 = iterround(wuvec, xstar, conds)
    # print('intxvec0: ')
    # printsol(intxvec0, bvars, bap["bundle_list_size"], bap["bundle_list"], tol)
    intxvec1 = iterround(-wuvec, xstar, conds)
    # print('intxvec1: ')
    # printsol(intxvec1, bvars, bap["bundle_list_size"], bap["bundle_list"], tol)
    Xmat = np.array([intxvec0, intxvec1]).T
    while True:
        ls = Xmat.shape[1]
        
        """@@@@@@@@@@@@@@ CVXOPT: quadprog @@@@@@@@@@@@@@@@ 
        PPP = matrix(np.dot(Xmat.T, Xmat), tc='d')
        qqq = matrix(-np.dot(Xmat.T, xstar), tc='d')
        AAA = matrix(np.ones((1, ls)), tc='d')
        bbb = matrix(1, tc='d')
        GGG = matrix(-np.eye(ls), tc='d')
        hhh = matrix(np.zeros(ls), tc='d')

        t0 = time.time()
        qsol = qp(PPP, qqq, GGG, hhh, AAA, bbb)
        dt = time.time() - t0
        if qsol["status"] != "optimal":
            raise SolverError("Quadprog " + qsol["status"])
        print('CVXOPT: time = {0:.2f} sec'.format(dt))
        la = np.ravel(qsol["x"])

        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"""
        # """ ------------- CVXPY: ECOS ----------------------
        varl = Variable(ls)
        PPP = np.dot(Xmat.T, Xmat)
        qqq = -np.dot(Xmat.T, xstar)
        AAA = np.ones(ls)
        q_cst = [AAA * varl == 1, varl >= 0]
        q_obj = Minimize(1.0/2 * quad_form(varl, PPP) + qqq * varl)
        q_prob = Problem(q_obj, q_cst)
        t0 = time.time()
        q_prob.solve(solver='ECOS', abstol=stol)
        dt = time.time() - t0
        if q_prob.status != 'optimal':
            raise SolverError("Quadprog " + q_prob.status)
        print('ECOS: time = {0:.2f} sec'.format(dt))
        la = np.ravel(varl.value)
        yvec = np.dot(Xmat, la)
        
        # sys.exit()
        # print('xstar: ')
        # printsol(xstar, bvars, bap["bundle_list_size"], bap["bundle_list"], tol) 

        xydiff = xstar - yvec
        # print('maxxydiff: ', np.max(np.abs(xydiff)))
        normxydiff = np.linalg.norm(xydiff)
        print('normxydiff: ', normxydiff)
        if normxydiff <= epsilon:
            break

        extremovelist = []
        for ii in range(ls):
            if la[ii] <= tol:
                extremovelist.append(ii)

        Xmat = np.delete(Xmat, extremovelist, axis=1)
        zvec = xstar + xdelta * xydiff / normxydiff

        zpvec = iterround(xydiff, zvec, conds)
        Xmat = np.c_[Xmat, zpvec]

    # Rollin' in the deep...
    xind = sum(np.random.rand() > np.cumsum(la))
    xvec = Xmat.T[xind]
    uval = np.dot(uvec, xvec)
    return Xmat, la, xvec, pstar, uval


def iterround(cvec, xvec, conds):
    """
        Iterative Rounding
        conds:{'Amat', 'Aeqmat', 'bvec', 'beqvec',
            'bagents_unsatnum', 'k', 'tol'}
    """
    print('Iterround')
    xunfixed_num = len(cvec)
    xunfixed_list = np.array(range(xunfixed_num), dtype=int)
    Amat = np.copy(conds["Amat"])
    bvec = np.copy(conds["bvec"])
    Aeqmat = np.copy(conds["Aeqmat"])
    beqvec = np.copy(conds["beqvec"])
    xvec = np.copy(xvec)
    bagents_unsatnum, k, tol = (conds[ss] for ss in ["bagents_unsatnum", "k", "tol"])

    firstiter = True
    part_xvec = xvec
    while True:
        # print("    Iterround iters")
        solved = True
        allfrac = True
        xelimlist = []
        xelim = []
        for ii in range(xunfixed_num):
            if np.abs(part_xvec[ii] - np.round(part_xvec[ii])) <= tol:
                allfrac = False
                xunfixed_num -= 1
                xelimlist.append(ii)
                xelim.append(round(part_xvec[ii]))
            else:
                solved = False

        xelim = np.array(xelim, dtype='int')
        if solved:
            return np.round(xvec)
        if not allfrac:
            cvec = np.delete(cvec, xelimlist)
            bvec = bvec - np.dot(np.take(Amat, xelimlist, axis=1), xelim)
            Amat = np.delete(Amat, xelimlist, axis=1)
            if beqvec.size > 0:
                beqvec = beqvec - np.dot(np.take(Aeqmat, xelimlist, axis=1), xelim)
                Aeqmat = np.delete(Aeqmat, xelimlist, axis=1)
                Aeqelimlist = np.flatnonzero(beqvec < tol)
                Aeqmat = np.delete(Aeqmat, Aeqelimlist, axis=0)
                beqvec = np.delete(beqvec, Aeqelimlist)
            
            Aelimlist = np.flatnonzero(np.sum(Amat, axis=1) < tol)
            bagents_unsatnum -= np.count_nonzero(Aelimlist < bagents_unsatnum)
            Amat = np.delete(Amat, Aelimlist, axis=0)
            bvec = np.delete(bvec, Aelimlist)

            xunfixed_list = np.delete(xunfixed_list, xelimlist)

        # print('allfrac: ', allfrac)
        if not firstiter and allfrac:
            jelimlist = []
            for jj in range(bagents_unsatnum, len(Amat)):
                if sum(Amat[jj]) <= bvec[jj] + k - 1 + tol:
                    jelimlist.append(jj)
            Amat = np.delete(Amat, jelimlist, axis=0)
            bvec = np.delete(bvec, jelimlist)

        # if not firstiter:
        # print('bagents_unsatnum: {4}, -cvec: {5}\n Amat:\n {0}\nbvec: {1}\nAeqmat:\n {2}\nbeqvec: {3}\n'.format(
            # Amat, bvec, Aeqmat, beqvec, bagents_unsatnum, -cvec))

        """@@@@@@@@@@@@@@@@@@@ CVXOPT: glpk @@@@@@@@@@@@@@@@@@@
        ccc = matrix(-cvec, tc='d')
        GGG = matrix(np.append(Amat, -np.eye(xunfixed_num), axis=0), tc='d')
        hhh = matrix(np.append(bvec, np.zeros(xunfixed_num)), tc='d')
        AAA = matrix(Aeqmat, tc='d')
        bbb = matrix(beqvec, tc='d')
        print("After that: GGG: {0}\nhhh: {1}".format(GGG, hhh))
        print("ccc: {0}, bagents_unsatnum: {1}".format(ccc, bagents_unsatnum))
        # 1/0
        newsol = lp(ccc, GGG, hhh, AAA, bbb, solver='glpk')  # Simplex: get extreme point
        if newsol["status"] != 'optimal':
            raise SolverError('Iterround')
        part_xvec = np.ravel(newsol["x"])
        @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@"""
        # ----------------- scipy.linprog -----------------
        # print(beqvec.size)
        if beqvec.size != 0:
            newsol = linprog(-cvec, A_ub=Amat, b_ub=bvec, A_eq=Aeqmat,
            b_eq=beqvec, options={"tol": tol*1e-2})
        else:
            newsol = linprog(-cvec, A_ub=Amat, b_ub=bvec, A_eq=None,
            b_eq=None, options={"tol": tol*1e-2})

        if not newsol["success"]:
            # print('bagents_unsatnum: {4}, Amat: {0}\nbvec: {1}\nAeqmat: {2}\nbeqvec: {3}\n'.format(
                # Amat, bvec, Aeqmat, beqvec, bagents_unsatnum))
            raise SolverError('Iterround {0}'.format(newsol["status"]))
        part_xvec = np.ravel(newsol["x"])
        # print('part_xvec:', part_xvec)
        # --------------------------------------------------

        # print(part_xvec)
        np.put(xvec, xunfixed_list, part_xvec)

        firstiter = False

def printsol(xvec, bvars, bundle_list_size, bundle_list, tol):
    """Print solution of LP"""
    for i in range(bvars):
        if xvec[i] > tol:
            print("Agent {0} is allocated {1:.3f} portion of bundle: {2}".format(
                i // bundle_list_size, xvec[i], bundle_list[i % bundle_list_size]))